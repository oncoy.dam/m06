package activitat2_4;

public class Moble {
    private double ample;
    private double llarg;
    private int color;

    public Moble(double ample, double llarg, int color) {
        this.ample = ample;
        this.llarg = llarg;
        this.color = color;
    }

    public void mostrarMueble() {
        System.out.println("Ancho: " + ample +
                "\nLargo: " + llarg +
                "\nColor: " + color + "\n");
    }

    public double getAmple() {
        return ample;
    }

    public double getLlarg() {
        return llarg;
    }

    public int getColor() {
        return color;
    }

}
