package activitat2_4;

import java.util.ArrayList;
import java.util.List;

public class Habitacio {
    private double ample;
    private double llarg;
    private String nom;
    private List<Moble> mobles;

    public Habitacio(double ample, double llarg, String nom) {
        this.ample = ample;
        this.llarg = llarg;
        this.nom = nom;
        this.mobles = new ArrayList<>();
    }

    public void agregarMuebles() {
        double ancho;
        double largo;
        int color;
        int respuesta;
        boolean continuar = true;
        while (continuar) {

            System.out.println("Introduzca los datos del mueble:");
            System.out.print("Ancho: ");
            ancho = Teclado.leerDouble();
            System.out.print("Largo: ");
            largo = Teclado.leerDouble();
            System.out.print("Color: ");
            color = Teclado.leerInt();

            Moble moble = new Moble(ancho, largo, color);
            mobles.add(moble);

            System.out.println("\nQuieres guardar otro mueble?\n1. Sí\n2. No");
            respuesta = Teclado.leerInt(1, 2);
            if (respuesta == 2) {
                continuar = false;
            }
        }
    }

    public void mostrarMuebles() {
        int contador = 1;
        if (mobles.isEmpty()) {
            System.out.println("No hay ningún mueble registrado.");
        } else {
            for (Moble moble : mobles) {
                System.out.println("Los datos del mueble " + contador + " son los siguientes:");
                moble.mostrarMueble();
                contador++;
            }
        }
    }

    public void mostrarDatos() {
        System.out.println("Datos de la habitación");
        System.out.println("Ancho = " + ample);
        System.out.println("Largo = " + llarg);
        System.out.println("Nombre = " + nom);
        System.out.println("Cantidad de muebles = " + mobles.size());
    }

    public double getAmple() {
        return ample;
    }

    public double getLlarg() {
        return llarg;
    }

    public String getNom() {
        return nom;
    }

    public List<Moble> getMobles() {
        return mobles;
    }

}
