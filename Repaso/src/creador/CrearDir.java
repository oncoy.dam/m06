package creador;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class CrearDir {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Introduce el nombre del primer directorio a crear: ");
        String dirA = sc.nextLine();
        File rutaA = new File("D:\\Documentos\\Repaso\\" + dirA);
        crearRuta(rutaA);

        System.out.print("\nIntroduce el nombre del segundo directorio a crear: ");
        String dirB = sc.nextLine();
        File rutaB = new File("D:\\Documentos\\Repaso\\" + dirB);
        crearRuta(rutaB);

    }

    public static void crearRuta(File file) {
        if (file.mkdir()) {
            System.out.println("Se ha creado el directorio con éxito");
            System.out.print("Cuántos ficheros quieres crear? ");
            int ficheros = sc.nextInt();
            System.out.print("Desde qué número quieres empezar? ");
            int num = sc.nextInt();
            sc.nextLine();
            for (int i = num; i < num+ficheros; i++) {
                File file1 = new File(file.getAbsolutePath() + "\\fitxer" + i + ".txt");
                try {
                    if (file1.createNewFile())
                        System.out.println("Creado el fichero " + i + " con éxito");
                } catch (IOException e) {
                    System.out.println("Ha habido un error al crear los archivos");
                }
            }
        }
    }
}
