package creador;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numRandom = randomNumber(), contador = 1, numIntroducido;
        boolean correcto = false;
        while (contador <= 3 && !correcto) {
            if (contador == 1 || contador == 2) {
                if (contador == 1)
                    System.out.print("FIRST ATTEMPT!! Enter first number: ");
                else
                    System.out.print("SECOND ATTEMPT!! Enter second number: ");
                numIntroducido = sc.nextInt();
                if (guessNumber(numIntroducido, numRandom) == -1)
                    System.out.print("Too low ");
                else if (guessNumber(numIntroducido, numRandom) == 0)
                    correcto = true;
                else
                    System.out.print("Too high ");
            } else {
                System.out.print("THIRD ATTEMPT!! Enter third number: ");
                numIntroducido = sc.nextInt();
                if (guessNumber(numIntroducido, numRandom) == 0)
                    correcto = true;
            }
            contador++;
        }
        if (correcto)
            System.out.println("YOU HAVE WON!! THE END.");
        else
            System.out.println("YOU HAVE LOST!! THE END.");
    }

    public static int randomNumber() {
        return (int) (Math.random() * (10 - 1 + 1) + 1);
    }

    public static int guessNumber(int numIntroducido, int num) {
        return Integer.compare(numIntroducido, num);
    }
}
