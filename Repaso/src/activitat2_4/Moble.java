package activitat2_4;

public class Moble {
    private double ample;
    private double llarg;
    private int color;

    public Moble(double ample, double llarg, int color) {
        this.ample = ample;
        this.llarg = llarg;
        this.color = color;
    }

    public void mostrarDatos() {
        System.out.println("Ample: " + ample);
        System.out.println("Llarg: " + llarg);
        System.out.println("Color: " + color);
    }

    public double getAmple() {
        return ample;
    }

    public double getLlarg() {
        return llarg;
    }

    public int getColor() {
        return color;
    }
}
