package activitat2_4;

import teclado.Leer;

public class Main {
    public static void main(String[] args) {
        String ruta_Prop = "D:\\Documentos\\Repaso\\hab.properties";
        String ruta_Hab = "D:\\Documentos\\Repaso\\hab.txt";
        Habitacio hab = Util.asignar_Hab(ruta_Hab, ruta_Prop);
        hab.guardarEnDisco(ruta_Hab);
        boolean seguir = true;
        while (seguir) {
            menu();
            System.out.print("Opció: ");
            switch (Leer.valorInt(0, 4)) {
                case 0:
                    seguir = false;
                    break;
                case 1:
                    hab.mostrarDatos();
                    break;
                case 2:
                    hab.afegirMoble();
                    break;
                case 3:
                    hab.guardarEnDisco(ruta_Hab);
                    break;
                case 4:
                    Habitacio hab2 = Util.leerDeDisco(ruta_Hab);
                    hab2.mostrarMuebles();
                    break;
            }
            System.out.println();
        }
    }

    public static void menu() {
        System.out.println("0. Sortir\n" +
                "1. Mostrar per pantalla les dades de l'habitació\n" +
                "2. Afegir un moble a l'habitació\n" +
                "\tEs demanaran les dades bàsiques (ample, llarg i color)\n" +
                "3. Guardar a disc l'habitació\n" +
                "4. Llistar per pantalla tots els mobles de l'habitació, amb totes les seves dades:\n" +
                "\tL'ample, llarg i el color");
    }
}
