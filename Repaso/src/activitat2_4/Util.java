package activitat2_4;

import java.io.*;
import java.util.Properties;

public class Util {
    public static Habitacio asignar_Hab(String ruta_Hab, String ruta_Properties) {
        File file_hab = new File(ruta_Hab);
        if (file_hab.exists()) {
            //Asignamos la habitación del disco en caso de que exista
            return leerDeDisco(ruta_Hab);
        } else {
            //Creamos una habitación nueva en caso de no existir
            return crearHabitacion(ruta_Hab, ruta_Properties);
        }
    }

    public static void crearProperties(String ruta_Properties) throws IOException {
        File file_prop = new File(ruta_Properties);
        Properties properties = new Properties();
        properties.setProperty("ample", "10.5");
        properties.setProperty("llarg", "5.0");
        properties.setProperty("nom", "Dormitorio");
        properties.store(new FileWriter(file_prop), "Guardado de datos de la habitación");
    }

    public static Habitacio crearHabitacion(String ruta_Hab, String ruta_Properties) {
        File file_prop = new File(ruta_Properties);
        Properties properties = new Properties();
        Habitacio habitacio = null;
        try {
            if (file_prop.createNewFile()) {
                //Creamos un fichero properties en caso de no existir
                crearProperties(ruta_Properties);
            }

            properties.load(new FileReader(file_prop));

            double ample = Double.parseDouble(properties.getProperty("ample"));
            double llarg = Double.parseDouble(properties.getProperty("llarg"));
            String nom = properties.getProperty("nom");
            habitacio = new Habitacio(ample, llarg, nom);
        } catch (IOException e) {
            System.out.println("Error al crear la habitación.");
        }
        return habitacio;
    }

    public static Habitacio leerDeDisco(String ruta) {
        Habitacio hab = null;
        try (DataInputStream lectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)))) {
            double ample = lectura.readDouble();
            double llarg = lectura.readDouble();
            String nom = lectura.readUTF();
            hab = new Habitacio(ample, llarg, nom);
            int num_vueltas = lectura.readInt();
            for (int i = 0; i < num_vueltas; i++) {
                double ample_moble = lectura.readDouble();
                double llarg_moble = lectura.readDouble();
                int color = lectura.readInt();
                hab.getMobles().add(new Moble(ample_moble, llarg_moble, color));
            }
        } catch (IOException e) {
            System.out.println("Error al leer los datos");
        }
        return hab;
    }
}
