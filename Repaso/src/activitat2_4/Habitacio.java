package activitat2_4;

import teclado.Leer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Habitacio {
    private double ample;
    private double llarg;
    private String nom;
    private List<Moble> mobles;

    public Habitacio(double ample, double llarg, String nom) {
        this.ample = ample;
        this.llarg = llarg;
        this.nom = nom;
        mobles = new ArrayList<>();
    }

    public void afegirMoble() {
        System.out.print("Ancho: ");
        double ample = Leer.valorDouble();
        System.out.print("Largo: ");
        double llarg = Leer.valorDouble();
        System.out.print("Color: ");
        int color = Leer.valorInt();
        mobles.add(new Moble(ample, llarg, color));
    }

    public void guardarEnDisco(String ruta) {
        try (DataOutputStream escritura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)))) {
            escritura.writeDouble(ample);
            escritura.writeDouble(llarg);
            escritura.writeUTF(nom);
            escritura.writeInt(mobles.size());
            for (Moble moble : mobles) {
                escritura.writeDouble(moble.getAmple());
                escritura.writeDouble(moble.getLlarg());
                escritura.writeInt(moble.getColor());
            }
            System.out.println("Se han guardado los datos correctamente.");
        } catch (IOException e) {
            System.out.println("Error al guardar en el disco");
        }
    }

    public void mostrarDatos() {
        System.out.println("\n=== Dades de l'habitació ===");
        System.out.println("Ample: " + ample);
        System.out.println("Llarg: " + llarg);
        System.out.println("Nom: " + nom);
        System.out.println("Número de mobles: " + mobles.size());
        System.out.println();
    }

    public void mostrarMuebles() {
        for (Moble moble : mobles) {
            System.out.println("\nDades del moble " + (mobles.indexOf(moble) + 1) + ":");
            moble.mostrarDatos();
        }
        System.out.println();
    }

    public List<Moble> getMobles() {
        return mobles;
    }
}
