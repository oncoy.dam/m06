package activitat3_2;

import java.util.ArrayList;
import java.util.List;

public class Assignatura {
    private int numero;
    private String nom;
    private int durada;
    private List<Alumne> alumnes;

    public Assignatura() {
        alumnes = new ArrayList<>();
    }

    public Assignatura(int numero, String nom, int durada) {
        this.numero = numero;
        this.nom = nom;
        this.durada = durada;
        alumnes = new ArrayList<>();
    }

    public void imprimir() {
        System.out.println("\nDatos de la asignatura:");
        System.out.println(" Número: " + numero);
        System.out.println(" Nom: " + nom);
        System.out.println(" Durada: " + durada);
        for (Alumne alumne : alumnes) {
            System.out.println(" Datos del alumno:");
            alumne.imprimir();
        }
    }


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDurada() {
        return durada;
    }

    public void setDurada(int durada) {
        this.durada = durada;
    }

    public List<Alumne> getAlumnes() {
        return alumnes;
    }

    public void setAlumnes(List<Alumne> alumnes) {
        this.alumnes = alumnes;
    }
}
