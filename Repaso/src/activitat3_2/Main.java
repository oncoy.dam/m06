package activitat3_2;

import teclado.Leer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Assignatura> assignaturas = null;
        boolean seguir = true;
        while (seguir) {
            menu();
            switch (Leer.valorInt(0, 6)) {
                case 0:
                    seguir = false;
                    break;
                case 1:
                    System.out.print("Indica el nombre del fichero: ");
                    assignaturas = Util.leerSecuencial(sc.nextLine());
                    break;
                case 2:
                    System.out.print("Indica el nombre del fichero: ");
                    assignaturas = Util.leerSintactico(sc.nextLine());
                    break;
                case 3:
                    if (assignaturas != null) {
                         for (Assignatura assignatura : assignaturas){
                            assignatura.imprimir();
                        }
                    }
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
            }
        }
    }

    public static void menu() {
        System.out.println("0. Sortir\n" +
                "1. Llegir d'un fitxer XML pel mètode seqüencial\n" +
                "\tEs demanarà el nom del fitxer\n" +
                "2. Llegir d'un fitxer XML pel mètode sintàctic\n" +
                "\tEs demanarà el nom del fitxer\n" +
                "3. Mostrar per pantalla totes les assignatures amb les seves dades (número, nom, durada i la llista d'alumnes)\n" +
                "4. Afegir una assignatura\n" +
                "\tEs demanaran les dades bàsiques (número, nom i durada)\n" +
                "5. Afegir un alumne a una assignatura\n" +
                "\tEs demanarà el número de l'assignatura (caldrà comprovar si existeix l'assignatura)\n" +
                "\tEs demanaran les de l'alumne (nom, dni i si és repetidor)\n" +
                "\tS'afegirà a la llista d'alumnes de l'assignatura\n" +
                "6. Guardar a disc en XML amb les assignatures");
    }
}
