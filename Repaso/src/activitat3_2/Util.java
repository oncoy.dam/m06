package activitat3_2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Util {
    static Scanner sc = new Scanner(System.in);

    public static void guardarSecuencial(List<Assignatura> lista_Assignaturas) {
        try {
            //Creamos un Document donde construiremos nuestro xml
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            //Creamos el elemento raíz con el nombre "assignaturas"
            Element raiz = doc.createElement("assignaturas");
            doc.appendChild(raiz);
            for (Assignatura assignatura : lista_Assignaturas) {
                Element e_Assignatura = doc.createElement("assignatura");
                raiz.appendChild(e_Assignatura);

                Element numero = doc.createElement("numero");
                numero.setTextContent(String.valueOf(assignatura.getNumero()));
                e_Assignatura.appendChild(numero);

                Element nom = doc.createElement("nom");
                nom.setTextContent(assignatura.getNom());
                e_Assignatura.appendChild(nom);

                Element durada = doc.createElement("durada");
                durada.setTextContent(String.valueOf(assignatura.getDurada()));
                e_Assignatura.appendChild(durada);

                Element e_alumnes = doc.createElement("alumnes");
                e_Assignatura.appendChild(e_alumnes);

                for (Alumne alumne : assignatura.getAlumnes()) {
                    Element e_alumne = doc.createElement("alumne");
                    e_alumnes.appendChild(e_alumne);

                    Element nom_alumne = doc.createElement("nom");
                    nom_alumne.setTextContent(alumne.getNom());
                    e_alumne.appendChild(nom_alumne);

                    Element dni_alumne = doc.createElement("dni");
                    dni_alumne.setTextContent(alumne.getDni());
                    e_alumne.appendChild(dni_alumne);

                    Element repetidor_alumne = doc.createElement("repetidor");
                    repetidor_alumne.setTextContent(String.valueOf(alumne.isRepetidor()));
                    e_alumne.appendChild(repetidor_alumne);
                }
            }

            System.out.print("Indica el nombre del fichero a guardar (nombre.xml): ");
            String ruta = sc.nextLine();

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(ruta));
            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.transform(source, result);

        } catch (TransformerException | ParserConfigurationException e) {
            System.out.println("Ha habido un error al guardar el fichero");
        }
    }

    public static List<Assignatura> leerSecuencial(String ruta) {
        List<Assignatura> lista_Assignaturas = new ArrayList<>();
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(ruta);
            Node raiz = doc.getDocumentElement();

            //Recorremos el nodo raíz <assignatures>
            NodeList assignatures = raiz.getChildNodes();
            for (int i = 0; i < assignatures.getLength(); i++) {
                //Entramos en cada tag <assignatura>
                if (assignatures.item(i).getNodeName().equals("assignatura")) {
                    Assignatura assignatura = new Assignatura();
                    //Recorremos el tag assignatura>
                    NodeList nL_Assignatura = assignatures.item(i).getChildNodes();
                    for (int j = 0; j < nL_Assignatura.getLength(); j++) {
                        switch (nL_Assignatura.item(j).getNodeName()) {
                            case "numero":
                                assignatura.setNumero(Integer.parseInt(nL_Assignatura.item(j).getTextContent()));
                                break;
                            case "nom":
                                assignatura.setNom(nL_Assignatura.item(j).getTextContent());
                                break;
                            case "durada":
                                assignatura.setDurada(Integer.parseInt(nL_Assignatura.item(j).getTextContent()));
                                break;
                            case "alumnes":
                                NodeList nL_Alumnes = nL_Assignatura.item(j).getChildNodes();
                                for (int k = 0; k < nL_Alumnes.getLength(); k++) {
                                    if (nL_Alumnes.item(k).getNodeName().equals("alumne")) {
                                        Alumne alumne = new Alumne();
                                        NodeList nL_Alumne = nL_Alumnes.item(k).getChildNodes();
                                        for (int l = 0; l < nL_Alumne.getLength(); l++) {
                                            switch (nL_Alumne.item(l).getNodeName()) {
                                                case "nom":
                                                    alumne.setNom(nL_Alumne.item(l).getTextContent());
                                                    break;
                                                case "dni":
                                                    alumne.setDni(nL_Alumne.item(l).getTextContent());
                                                    break;
                                                case "repetidor":
                                                    alumne.setRepetidor(Boolean.parseBoolean(nL_Alumne.item(l).getTextContent()));
                                                    break;
                                            }
                                        }
                                        assignatura.getAlumnes().add(alumne);
                                    }
                                }
                                break;
                        }
                    }
                    lista_Assignaturas.add(assignatura);
                }
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return lista_Assignaturas;
    }

    public static List<Assignatura> leerSintactico(String ruta) {
        List<Assignatura> lista_Assignaturas = new ArrayList<>();
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(ruta);
            NodeList nL_Assignatures = devolverNodeList(doc, "/assignatures/assignatura");

            for (int i = 0; i < nL_Assignatures.getLength(); i++) {

                int numero = Integer.parseInt(devolverNodeList(nL_Assignatures.item(i), "numero").item(0).getTextContent());
                String nom = devolverNodeList(nL_Assignatures.item(i), "nom").item(0).getTextContent();
                int durada = Integer.parseInt(devolverNodeList(nL_Assignatures.item(i), "durada").item(0).getTextContent());

                Assignatura assignatura = new Assignatura(numero, nom, durada);

                NodeList nL_Alumne = devolverNodeList(nL_Assignatures.item(i), "alumnes/alumne");
                for (int j = 0; j < nL_Alumne.getLength(); j++) {
                    String nom_alumne = devolverNodeList(nL_Alumne.item(j), "nom").item(0).getTextContent();
                    String dni = devolverNodeList(nL_Alumne.item(j), "dni").item(0).getTextContent();
                    boolean repetidor = Boolean.parseBoolean(devolverNodeList(nL_Alumne.item(j), "repetidor").item(0).getTextContent());

                    assignatura.getAlumnes().add(new Alumne(nom_alumne, dni, repetidor));
                }
                lista_Assignaturas.add(assignatura);
            }
        } catch (SAXException | IOException | ParserConfigurationException | XPathExpressionException e) {
            e.printStackTrace();
        }

        return lista_Assignaturas;
    }

    private static NodeList devolverNodeList(Object obj, String tag) throws XPathExpressionException {
        XPathExpression expr = XPathFactory.newInstance().newXPath().compile(tag);
        return (NodeList) expr.evaluate(obj, XPathConstants.NODESET);
    }
}
