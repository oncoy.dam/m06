package activitat1_2;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        String ruta = "D:\\Documentos\\Repaso\\DIR";
        File dir = new File(ruta);

        if (dir.exists()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    borrar(file);
                }
                borrar(dir);
            }
        }
    }

    public static void borrar(File file) {
        if (file.isDirectory()) {
            if (file.delete()) {
                System.out.println("S'ha esborrat el directori " + file.getName());
            } else {
                System.out.println("No s'ha pogut esborrar el directori " + file.getName());
            }
        } else {
            if (file.delete()) {
                System.out.println("S'ha esborrat el fitxer " + file.getName());
            } else {
                System.out.println("No s'ha pogut esborrar el fitxer " + file.getName());
            }
        }
    }
}
