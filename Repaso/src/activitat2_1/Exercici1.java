package activitat2_1;

import java.io.*;

public class Exercici1 {
    // Fes un únic programa que emmagatzemi en un fitxer, per bytes, i posteriorment recuperi i pinti per pantalla les següents dades una darrera l'altra:
    //Un booleà
    //Un enter (superior a 100000)
    //Un conjunt de caràcters

    public static void main(String[] args) {

        try {
            DataOutputStream escritura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("D:\\Documentos\\Repaso\\fitx.txt")));
            escritura.writeBoolean(true);
            escritura.writeInt(10);
            escritura.writeUTF("Hola");
            escritura.close();

            DataInputStream lectura = new DataInputStream(new BufferedInputStream(new FileInputStream("D:\\Documentos\\Repaso\\fitx.txt")));
            System.out.println(lectura.readBoolean() + " " + lectura.readInt() + " " + lectura.readUTF());
            lectura.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
