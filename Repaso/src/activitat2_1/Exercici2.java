package activitat2_1;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Exercici2 {
    public static void main(String[] args) {
        try {
            OutputStreamWriter escritura = new OutputStreamWriter(new FileOutputStream("D:\\Documentos\\Repaso\\fitxer1.txt", true), StandardCharsets.ISO_8859_1);
            escritura.write("5");
            escritura.write("hola");
            escritura.close();
            InputStreamReader lectura = new InputStreamReader(new FileInputStream("D:\\Documentos\\Repaso\\fitxer1.txt"), StandardCharsets.ISO_8859_1);

            int charLeido;
            while ((charLeido = lectura.read()) != -1) {
                System.out.print((char) charLeido);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}