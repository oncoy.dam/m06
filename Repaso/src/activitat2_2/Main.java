package activitat2_2;

import teclado.Leer;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        boolean seguir = true;
        while (seguir) {
            System.out.println("\nEscoja una opción:\n 0. Sortir \n 1. Escriure enters\n 2. Llegir enters");
            switch (Leer.valorInt(0, 2)) {
                case 0:
                    seguir = false;
                    System.out.println("El programa ha finalitzat");
                    break;
                case 1:
                    escriureEnters();
                    break;
                case 2:
                    llegirEnters();
                    break;
            }
        }
    }

    public static void escriureEnters() {
        try (DataOutputStream escritura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("D:\\Documentos\\Repaso\\fitxer1.txt")))) {
            System.out.println("¿Cuántos enteros quiere guardar?");
            int[] array = new int[Leer.valorInt()];
            escritura.writeInt(array.length);

            for (int i = 0; i < array.length; i++) {
                System.out.print("Introduzca un número a guardar: ");
                array[i] = Leer.valorInt();
                escritura.writeInt(array[i]);
            }
        } catch (IOException e) {
            System.out.println("Error al guardar.");
        }
    }

    public static void llegirEnters() {
        try (DataInputStream lectura = new DataInputStream(new BufferedInputStream(new FileInputStream("D:\\Documentos\\Repaso\\fitxer1.txt")))) {
            System.out.println("Mostrando los números guardados: ");
            int[] array = new int[lectura.readInt()];

            for (int i = 0; i < array.length; i++) {
                array[i] = lectura.readInt();
                System.out.print(array[i] + " ");
            }

            System.out.println();
        } catch (IOException e) {
            System.out.println("Error al leer");
        }

    }
}
