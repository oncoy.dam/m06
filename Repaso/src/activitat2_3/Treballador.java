package activitat2_3;

import java.io.*;

public class Treballador implements Serializable {
    private String nif;
    private String nom;
    private int sou;

    public Treballador(String nif, String nom, int sou) {
        this.nif = nif;
        this.nom = nom;
        this.sou = sou;
    }

    public void mostrarDatos() {
        System.out.println("\nDatos del trabajador");
        System.out.println("NIF: " + nif);
        System.out.println("Nombre: " + nom);
        System.out.println("Sueldo: " + sou);
    }

    public void guardar(String ruta) {
        try (ObjectOutputStream guardar = new ObjectOutputStream(new FileOutputStream(ruta))){
            guardar.writeObject(this);
        }catch (IOException e) {
            System.out.println("Error al guardar");
        }
    }

    public void guardarV2(String ruta) {
        try (DataOutputStream guardar = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)))) {
            guardar.writeUTF(nif);
            guardar.writeUTF(nom);
            guardar.writeInt(sou);
        } catch (IOException e) {
            System.out.println("Error al guardar");
        }
    }
}
