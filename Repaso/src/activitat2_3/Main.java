package activitat2_3;

import teclado.Leer;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Treballador treballador;
        String ruta = "D:\\Documentos\\Repaso\\fitxer1.txt";
        boolean seguir = true;
        while (seguir) {
            System.out.println("\n1. Guardar datos\n2. Recuperar datos\n3. Guardar datos binario\n4. Recuperar datos binario");
            System.out.print("Opción: ");

            switch (Leer.valorInt(0, 4)) {
                case 0:
                    seguir = false;
                    break;
                case 1:
                    treballador = pedirTrabajador();
                    treballador.guardar(ruta);
                    break;
                case 2:
                    treballador = leerTrabajador(ruta);
                    treballador.mostrarDatos();
                    break;
                case 3:
                    treballador = pedirTrabajador();
                    treballador.guardarV2(ruta);
                    break;
                case 4:
                    treballador = leerTrabajadorV2(ruta);
                    treballador.mostrarDatos();
            }
        }
    }

    public static Treballador pedirTrabajador() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca el nif: ");
        String nif = sc.nextLine();
        System.out.print("Introduzca el nombre: ");
        String nombre = sc.nextLine();
        System.out.print("Introduzca el sueldo: ");
        int sueldo = Leer.valorInt();

        return new Treballador(nif, nombre, sueldo);
    }

    public static Treballador leerTrabajador(String ruta) {
        Treballador treballador = null;
        try (ObjectInputStream leer = new ObjectInputStream(new FileInputStream(ruta))) {
            treballador = (Treballador) leer.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Error al leer");
        }
        return treballador;
    }

    public static Treballador leerTrabajadorV2(String ruta) {
        Treballador treballador = null;
        try (DataInputStream leer = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)))) {
            treballador = new Treballador(leer.readUTF(), leer.readUTF(), leer.readInt());
        }catch (IOException e) {
            System.out.println("Error al leer");
        }
        return treballador;
    }
}
