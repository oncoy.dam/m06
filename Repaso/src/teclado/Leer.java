package teclado;

import java.util.Scanner;

public class Leer {
    static Scanner sc = new Scanner(System.in);

    public static int valorInt() {
        while (true) {
            String num = sc.nextLine();
            try {
                return Integer.parseInt(num);
            } catch (NumberFormatException e) {
                System.out.print("Valor incorrecto. Introduzca un número entero: ");
            }
        }
    }

    public static int valorInt(int min, int max) {
        while (true) {
            int num = valorInt();
            if (num >= min && num <= max)
                return num;
            else
                System.out.printf("Número incorrecto. Introduzca un número entre %d y %d", min, max);
        }
    }

    public static double valorDouble() {
        while (true) {
            String num = sc.nextLine();
            try {
                return Double.parseDouble(num);
            } catch (NumberFormatException e) {
                System.out.print("Valor incorrecto. Introduzca un double: ");
            }
        }
    }
}
