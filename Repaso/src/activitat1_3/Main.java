package activitat1_3;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        File rutaA = new File("D:\\Documentos\\Repaso\\DirA");
        File rutaB = new File("D:\\Documentos\\Repaso\\DirB");

        System.out.println("S'estan comparant els següents directoris:");
        System.out.println("--- Directori A: " + rutaA.getPath());
        System.out.println("--- Directori B: " + rutaB.getPath());

        File[] filesA = rutaA.listFiles();
        File[] filesB = rutaB.listFiles();

        boolean repetit;
        if (filesA != null && filesB != null) {
            for (File fileA : filesA) {
                repetit = false;
                for (File fileB : filesB) {
                    if (fileA.getName().equals(fileB.getName())) {
                        repetit = true;
                        System.out.println("-El fitxer " + fileA.getName() + " existeis en ambdós directoris");
                        compararData(fileA, fileB);
                        compararTamany(fileA, fileB);
                        break;
                    }
                }
                if (!repetit) {
                    System.out.println("-El fitxer " + fileA.getName() + " existeix en el directori A, però no en el B");
                }
            }
        }
    }

    private static void compararTamany(File fileA, File fileB) {
        if (fileA.length() > fileB.length()) {
            System.out.println("---Pesa més al directori A");
        } else if (fileA.length() < fileB.length()) {
            System.out.println("---Pesa més al directori B");
        } else {
            System.out.println("---Tenen el mateix pes");
        }
    }

    private static void compararData(File fileA, File fileB) {
        if (fileA.lastModified() > fileB.lastModified()) {
            System.out.println("---S'ha modificat més recentment al directori A");
        } else if (fileA.lastModified() < fileB.lastModified()) {
            System.out.println("---S'ha modificat més recentment al directori B");
        } else {
            System.out.println("---Tenen la mateixa data");
        }
    }
}
