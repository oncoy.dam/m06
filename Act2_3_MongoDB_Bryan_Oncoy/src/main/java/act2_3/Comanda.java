package act2_3;

import java.time.LocalDate;

public class Comanda {
    private LocalDate data_comanda;
    private int _import;
    private boolean pagada;

    public Comanda(LocalDate data_comanda, int _import, boolean pagada) {
        this.data_comanda = data_comanda;
        this._import = _import;
        this.pagada = pagada;
    }

    public Comanda(LocalDate data_comanda, int _import) {
        this.data_comanda = data_comanda;
        this._import = _import;
    }

    @Override
    public String toString() {
        return "Comanda {data: " + data_comanda + ", import: " + _import + ", pagada: " + pagada +"}";
    }

    public LocalDate getData_comanda() {
        return data_comanda;
    }

    public int get_import() {
        return _import;
    }

    public boolean isPagada() {
        return pagada;
    }
}
