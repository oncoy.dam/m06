package act2_3;

public class Main {
    public static void main(String[] args) {
        boolean continuar = true;
        while (continuar) {
            System.out.println("\n0) Sortir \n1) Donar d'alta un client \n2) Afegir comandes a un client \n3) Cercar clients per facturació \n4) Cercar clients per quantitat de comandes");
            System.out.print("Seleccioni una opció: ");
            int opcio = Teclado.leerInt(0, 4);
            switch (opcio) {
                case 0:
                    System.out.println("El programa ha finalitzat.");
                    continuar = false;
                    break;
                case 1:
                    exercici2();
                    break;
                case 2:
                    exercici3i4();
                    break;
                case 3:
                    exercici5();
                    break;
                case 4:
                    exercici6();
                    break;
            }
        }
    }

    public static void exercici1() {
        Client client = new Client("12345678X", "Pere Pons", 0);
        MongoClient.insertar_v1(client);
    }

    public static void exercici2() {
        MongoClient.insertar_v2(demanarDadesClient());
    }

    public static void exercici3i4() {
        MongoClient.listarNIF();
    }

    public static void exercici5() {
        System.out.print("Introdueixi la facturació mínima a buscar: ");
        int facturacio = Teclado.leerInt();
        MongoClient.buscarClientsPerFacturacio(facturacio);
    }

    public static void exercici6() {
        System.out.print("Introdueixi la quantitat de comandes mínima a buscar: ");
        int quantitat = Teclado.leerInt();
        MongoClient.buscarClientsPerComandes(quantitat);
    }

    public static Client demanarDadesClient() {
        System.out.print("Introdueixi el nif del client: ");
        String nif = Teclado.leerStr();
        System.out.print("Introdueixi el nom del client: ");
        String nom = Teclado.leerStr();
        System.out.print("Introdueixi el total_facturacio del client: ");
        int total_facturacio = Teclado.leerInt();
        System.out.print("Introdueixi el telefon del client: ");
        String telefon = Teclado.StringOrNull();
        System.out.print("Introdueixi el correu del client: ");
        String correu = Teclado.StringOrNull();

        return new Client(nif, nom, total_facturacio, telefon, correu);
    }
}
