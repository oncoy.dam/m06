package act2_3;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;

public class MongoClient {

    public static void insertar_v1(Client client) {
        try (com.mongodb.client.MongoClient mongoClient = MongoClients.create();) {
            MongoDatabase database = mongoClient.getDatabase("db_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            Document doc = new Document("nif", client.getNif()).append("nom", client.getNom()).append("total_facturacio", client.getTotal_facturacio()).append("telefon", client.getTelefon()).append("correu", client.getCorreu());
            List<Document> arrayComandes = new ArrayList<>();
            for (Comanda comanda : client.getComandes()) {
                arrayComandes.add(new Document("data_comanda", comanda.getData_comanda()).append("import", comanda.get_import()).append("pagada", comanda.isPagada()));
            }
            doc.append("comandes", arrayComandes);
            collection.insertOne(doc);
        }
    }

    public static void insertar_v2(Client client) {
        try (com.mongodb.client.MongoClient mongoClient = MongoClients.create();) {
            MongoDatabase database = mongoClient.getDatabase("db_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            Document doc = new Document("nif", client.getNif()).append("nom", client.getNom()).append("total_facturacio", client.getTotal_facturacio());
            if (client.getTelefon() != null)
                doc.append("telefon", client.getTelefon());
            if (client.getCorreu() != null)
                doc.append("correu", client.getCorreu());
            if (client.getComandes().size() != 0) {
                List<Document> arrayComandes = new ArrayList<>();
                for (Comanda comanda : client.getComandes()) {
                    arrayComandes.add(new Document("data_comanda", comanda.getData_comanda()).append("import", comanda.get_import()).append("pagada", comanda.isPagada()));
                }
                doc.append("comandes", arrayComandes);
            }
            collection.insertOne(doc);
        }
    }

    public static void listarNIF() {
        List<String> nifs = new ArrayList<>();

        try (com.mongodb.client.MongoClient mongoClient = MongoClients.create();) {
            MongoDatabase database = mongoClient.getDatabase("db_prova");
            MongoCollection<Document> collection = database.getCollection("clients");
            try (MongoCursor<Document> cursor = collection.find().iterator()) {
                while (cursor.hasNext()) {
                    Document d = cursor.next();
                    nifs.add(d.getString("nif"));
                }
            }

            if (nifs.size() != 0) {
                int contador = 1;
                System.out.println("Llista de NIFs");
                for (String nif : nifs) {
                    System.out.println(contador + ") " + nif);
                    contador++;
                }
                System.out.print("Seleccioni un: ");
                int index = Teclado.leerInt(1, nifs.size()) - 1;

                try (MongoCursor<Document> cursor = collection.find(eq("nif", nifs.get(index))).iterator()) {
                    while (cursor.hasNext()) {
                        Document d = cursor.next();
                        Client client = recuperarClient(d);

                        insertarComanda(client);
                        client.actualitzarFacturacio();

                        eliminarClientPerNif(client.getNif());
                        insertar_v2(client);
                    }
                }
            }
        }
    }

    public static void buscarClientsPerFacturacio(int facturacio_total) {
        try (com.mongodb.client.MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("db_prova");
            MongoCollection<Document> collection = database.getCollection("clients");
            try (MongoCursor<Document> cursor = collection.find(gt("total_facturacio", facturacio_total)).iterator()) {
                while (cursor.hasNext()) {
                    Document d = cursor.next();
                    Client client = recuperarClient(d);
                    client.mostrarClient();
                }
            }
        }
    }

    public static void buscarClientsPerComandes(int quantitat_comandes) {
        try (com.mongodb.client.MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("db_prova");
            MongoCollection<Document> collection = database.getCollection("clients");
            try (MongoCursor<Document> cursor = collection.find(where("this.comandes.length > " + quantitat_comandes)).iterator()) {
                while (cursor.hasNext()) {
                    Document d = cursor.next();
                    Client client = recuperarClient(d);
                    client.mostrarClient();
                }
            }
        }
    }

    public static void insertarComanda(Client client) {
        System.out.println("Introdueix una nova comanda");
        System.out.print("Data de facturació: ");
        LocalDate data_comanda = Teclado.leerDate();
        System.out.print("Import: ");
        int _import = Teclado.leerInt();
        client.getComandes().add(new Comanda(data_comanda, _import));
    }

    public static void eliminarClientPerNif(String nif) {
        try (com.mongodb.client.MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("db_prova");
            MongoCollection<Document> collection = database.getCollection("clients");
            DeleteResult deleteResult = collection.deleteMany(eq("nif", nif));
        }
    }

    public static Client recuperarClient(Document d) {
        String nif = d.getString("nif");
        String nom = d.getString("nom");
        int total_facturacio = d.getInteger("total_facturacio");
        String telefon = d.getString("telefon");
        String correu = d.getString("correu");

        Client client = new Client(nif, nom, total_facturacio, telefon, correu);

        List<Document> llistaComandes = d.getList("comandes", Document.class);
        if (llistaComandes != null) {
            SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy");
            for (Document comanda : llistaComandes) {
                String data = sdt.format(comanda.getDate("data_comanda"));
                LocalDate data_comanda = LocalDate.parse(data, Teclado.formatter);
                int _import = comanda.getInteger("import");
                boolean pagada = comanda.getBoolean("pagada");
                client.getComandes().add(new Comanda(data_comanda, _import, pagada));
            }
        }

        return client;
    }
}
