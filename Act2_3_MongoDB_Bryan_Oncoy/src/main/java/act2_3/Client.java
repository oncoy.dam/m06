package act2_3;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private String nif;
    private String nom;
    private int total_facturacio;
    private String telefon;
    private String correu;
    private List<Comanda> comandes;

    public Client(String nif, String nom, int total_facturacio) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
        comandes = new ArrayList<>();
    }

    public Client(String nif, String nom, int total_facturacio, String telefon, String correu) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
        this.telefon = telefon;
        this.correu = correu;
        comandes = new ArrayList<>();
    }

    public void mostrarClient() {
        System.out.printf("\nCLIENT nif: %s, nom %s, total_facturacio: %d, telefon: %s, correu: %s, comandes: %s\n", nif, nom, total_facturacio, telefon, correu, comandes);

    }

    public void actualitzarFacturacio() {
        int total = 0;
        for (Comanda comanda : comandes) {
            total += comanda.get_import();
        }
        total_facturacio = total;
    }

    public String getNif() {
        return nif;
    }

    public String getNom() {
        return nom;
    }

    public int getTotal_facturacio() {
        return total_facturacio;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getCorreu() {
        return correu;
    }

    public List<Comanda> getComandes() {
        return comandes;
    }
}
