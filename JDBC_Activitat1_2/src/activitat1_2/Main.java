package activitat1_2;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Main {
    static Scanner sc = new Scanner(System.in);
    static SentenciaSQL sentenciaSQL = new SentenciaSQL();

    public static void main(String[] args) {
        boolean repetir = true;
        int opcio;
        Map<Integer, Client> clients = new HashMap<>();

        while (repetir) {
            System.out.println("\nSeleccioni una opció: \n" +
                    "0. Sortir \n" +
                    "1. Creació del model \n" +
                    "2. Recuperació de les dades de la BBDD \n" +
                    "3. Emmagatzemat de dades a la BBDD \n" +
                    "4. Alta d'un nou client \n" +
                    "5. Alta d'una nova comanda \n" +
                    "6. Mostrar les comandes d'un client");
            opcio = Teclado.leerInt(0, 6);
            switch (opcio) {
                case 0:
                    repetir = false;
                    break;
                case 1:
                    sentenciaSQL.crearTaules();
                    break;
                case 2:
                    sentenciaSQL.llegir(clients);
                    break;
                case 3:
                    sentenciaSQL.emmagatzemar(clients);
                    break;
                case 4:
                    registrarClient(clients);
                    break;
                case 5:
                    registrarComanda(clients);
                    break;
                case 6:
                    mostrarComandes(clients);
                    break;
            }
        }
    }

    public static void registrarClient(Map<Integer, Client> clients) {
        System.out.print("Introdueix el nom: ");
        String nom = sc.nextLine();

        int dni;
        boolean continuar;
        do {
            continuar = false;
            System.out.print("Introdueix el DNI: ");
            dni = Teclado.leerInt();
            String medida = String.valueOf(dni);
            if (medida.length() == 8) {
                if (clients.containsKey(dni)) {
                    System.out.print("Aquest número de DNI ja està registrat. Introdueixi un altre: ");
                    continuar = true;
                }
            } else {
                System.out.println("El DNI ha de constar de 8 dígits.");
                continuar = true;
            }
        } while (continuar);

        System.out.print("És un client premium? (S/N): ");
        boolean premium = Teclado.leerSN();

        clients.put(dni, new Client(dni, nom, premium));
    }

    public static void registrarComanda(Map<Integer, Client> clients) {
        if (clients.isEmpty()) {
            System.out.println("S'ha de registrar un client abans de fer una comanda.");
        } else {
            System.out.println("Aquests són els clients: ");
            int contador = 0;
            for (Client client : clients.values()) {
                contador++;
                System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            }

            System.out.print("Seleccioneu un client: ");
            List<Integer> llistaDni = new ArrayList<>(clients.keySet());
            int clientDni = llistaDni.get(Teclado.leerInt(1, contador) - 1);

            System.out.print("Introdueixi el número de la comanda: ");
            int numComanda;
            boolean continuar;
            do {
                continuar = false;
                numComanda = Teclado.leerInt();
                for (Client client : clients.values()) {
                    if (client.comandes.containsKey(numComanda)) {
                        System.out.print("Aquest número de comanda ja està registrat. Introdueixi un altre: ");
                        continuar = true;
                        break;
                    }
                }
            } while (continuar);

            System.out.print("Introdueixi el preu: ");
            float preu = Teclado.leerFloat();

            System.out.print("Introdueixi la data: ");
            LocalDate data;
            while (true) {
                String dataIntroduida = sc.nextLine();
                try {
                    data = LocalDate.parse(dataIntroduida, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                    break;
                } catch (DateTimeException e) {
                    System.out.print("La data és incorrecta. El format es dd/mm/aaaa: ");
                }
            }

            clients.get(clientDni).comandes.put(numComanda, new Comanda(numComanda, preu, data, clientDni));
        }
    }

    public static void mostrarComandes(Map<Integer, Client> clients) {
        if (clients.isEmpty()) {
            System.out.println("No hi ha cap client registrat.");
        } else {
            System.out.println("Aquests són els clients: ");
            int contador = 0;
            for (Client client : clients.values()) {
                contador++;
                System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            }

            System.out.print("Seleccioneu un client: ");
            List<Integer> llistaDni = new ArrayList<>(clients.keySet());
            int clientDNI = llistaDni.get(Teclado.leerInt(1, contador) - 1);

            for (Comanda comanda : clients.get(clientDNI).comandes.values()) {
                comanda.imprimir();
            }
        }
    }
}
