package activitat1_2;

import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public class Main {
    static Scanner sc = new Scanner(System.in);
    static SentenciaSQL sentenciaSQL = new SentenciaSQL();

    public static void main(String[] args) {
        boolean repetir = true;
        int opcio;
        Map<Integer, Client> clients = new HashMap<>();
        sentenciaSQL.llegir(clients);

        while (repetir) {
            System.out.println("\nSeleccioni una opció: \n" +
                    "0. Sortir \n" +
                    "1. Eliminar un client i totes les seves comandes \n" +
                    "2. Actualitzar les dades d'un client \n" +
                    "3. Mostrar clients que comencen per... \n" +
                    "4. Alta d'un nou client \n" +
                    "5. Alta d'una nova comanda \n" +
                    "6. Mostrar les comandes d'un client \n" +
                    "7. Generació de resum de facturació");
            opcio = Teclado.leerInt(0, 7);
            switch (opcio) {
                case 0:
                    repetir = false;
                    sentenciaSQL.close();
                    break;
                case 1:
                    eliminar_client(clients);
                    sentenciaSQL.llegir(clients);
                    break;
                case 2:
                    actualitzar_dades(clients);
                    break;
                case 3:
                    mostrarCoincidencies();
                    break;
                case 4:
                    registrarClient(clients);
                    break;
                case 5:
                    registrarComanda(clients);
                    break;
                case 6:
                    mostrarComandes(clients);
                    break;
                case 7:
                    crear_resum();
                    break;
            }
        }
    }

    public static void registrarClient(Map<Integer, Client> clients) {
        System.out.print("Introdueix el nom: ");
        String nom = sc.nextLine();

        int dni;
        boolean continuar;
        do {
            continuar = false;
            System.out.print("Introdueix el DNI: ");
            dni = Teclado.leerInt();
            String medida = String.valueOf(dni);
            if (medida.length() == 8) {
                if (clients.containsKey(dni)) {
                    System.out.print("Aquest número de DNI ja està registrat. Introdueixi un altre: ");
                    continuar = true;
                }
            } else {
                System.out.println("El DNI ha de constar de 8 dígits.");
                continuar = true;
            }
        } while (continuar);

        System.out.print("És un client premium? (S/N): ");
        boolean premium = Teclado.leerSN();

        Client client = new Client(dni, nom, premium);
        sentenciaSQL.emmagatzemar_client(client);
        clients.put(dni, client);
    }

    public static void registrarComanda(Map<Integer, Client> clients) {
        if (clients.isEmpty()) {
            System.out.println("S'ha de registrar un client abans de fer una comanda.");
        } else {
            System.out.println("Aquests són els clients: ");
            int contador = 0;
            for (Client client : clients.values()) {
                contador++;
                System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            }

            System.out.print("Seleccioneu un client: ");
            List<Integer> llistaDni = new ArrayList<>(clients.keySet());
            int clientDni = llistaDni.get(Teclado.leerInt(1, contador) - 1);

            System.out.print("Introdueixi el número de la comanda: ");
            int numComanda;
            boolean continuar;
            do {
                continuar = false;
                numComanda = Teclado.leerInt();
                for (Client client : clients.values()) {
                    if (client.comandes.containsKey(numComanda)) {
                        System.out.print("Aquest número de comanda ja està registrat. Introdueixi un altre: ");
                        continuar = true;
                        break;
                    }
                }
            } while (continuar);

            System.out.print("Introdueixi el preu: ");
            float preu = Teclado.leerFloat();

            System.out.print("Introdueixi la data: ");
            LocalDate data;
            while (true) {
                String dataIntroduida = sc.nextLine();
                try {
                    data = LocalDate.parse(dataIntroduida, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                    break;
                } catch (DateTimeException e) {
                    System.out.print("La data és incorrecta. El format es dd/mm/aaaa: ");
                }
            }

            Comanda comanda = new Comanda(numComanda, preu, data, clientDni);
            sentenciaSQL.emmagatzemar_comanda(comanda);
            clients.get(clientDni).comandes.put(numComanda, comanda);
        }
    }

    public static void mostrarComandes(Map<Integer, Client> clients) {
        int clientDNI = llistarDni(clients);
        if (clientDNI != -1) {
            for (Comanda comanda : clients.get(clientDNI).comandes.values()) {
                comanda.imprimir();
            }
        }
    }

    public static void eliminar_client(Map<Integer, Client> clients) {
        int clientDNI = llistarDni(clients);
        if (clientDNI != -1) {
            sentenciaSQL.esborrar(clientDNI);
        }
    }

    public static int llistarDni(Map<Integer, Client> clients) {
        if (clients.isEmpty()) {
            System.out.println("No hi ha cap client registrat.");
            return -1;
        } else {
            System.out.println("Aquests són els clients: ");
            int contador = 0;
            for (Client client : clients.values()) {
                contador++;
                System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            }

            System.out.print("Seleccioneu un client: ");
            List<Integer> llistaDni = new ArrayList<>(clients.keySet());
            return llistaDni.get(Teclado.leerInt(1, contador) - 1);
        }
    }

    public static void actualitzar_dades(Map<Integer, Client> clients) {
        int clientDNI = llistarDni(clients);
        if (clientDNI != -1) {
            System.out.print("Introdueixi el nou nom: ");
            String nom = sc.nextLine();
            System.out.print("Es premium? (S/N): ");
            boolean premium = Teclado.leerSN();
            System.out.println(premium);
            sentenciaSQL.actualizar(clientDNI, nom, premium);
        }
    }

    public static void mostrarCoincidencies() {
        System.out.print("Introdueixi el començament del nom: ");
        String nom = sc.nextLine();
        sentenciaSQL.mostrar(nom);
    }

    public static void crear_resum() {
        System.out.print("Introdueixi el mes: ");
        int mes = Teclado.leerInt(1, 12);
        while (true) {
            System.out.print("Introdueixi l'any: ");
            int any = Teclado.leerInt();
            String data = "01/01/" + any;
            try {
                LocalDate date = LocalDate.parse(data, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                sentenciaSQL.generar_resum(mes, any);

                break;
            } catch (DateTimeException e) {
                System.out.print("L'any no es vàlid. ");
            }
        }
    }
}
