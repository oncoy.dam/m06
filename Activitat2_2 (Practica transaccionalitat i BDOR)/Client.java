package activitat1_2;

import java.util.HashMap;
import java.util.Map;

public class Client {
    int dni;
    String nom;
    boolean premium;
    Map<Integer, Comanda> comandes;

    public Client() {
        comandes = new HashMap<>();
    }

    public Client(int dni, String nom, boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
        this.comandes = new HashMap<>();
    }

}
