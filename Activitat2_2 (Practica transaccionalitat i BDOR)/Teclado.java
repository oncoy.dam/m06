package activitat1_2;

import java.util.Scanner;

public class Teclado {
    private static Scanner sc = new Scanner(System.in);
    private static boolean error;

    //Método para leer un entero filtrando errores
    public static int leerInt() {
        while (true) {
            try {
                return Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.print("Has ingresado un valor incorrecto. Ingresa un número entero: ");
            }
        }
    }

    //Método para leer un entero entre un rango de números
    public static int leerInt(int min, int max) {
        int valor;
        while (true) {
            valor = leerInt();
            if (valor < min || valor > max) {
                System.out.println("Opción escogida no válida.");
                System.out.print("Ingrese un valor entre " + min + " y " + max + ": ");
            } else {
                return valor;
            }
        }
    }

    //Método para leer un S/N
    public static boolean leerSN() {
        String respuesta;
        while (true) {
            respuesta = sc.nextLine().toLowerCase();
            if (respuesta.compareTo("s") == 0) {
                return true;
            } else if (respuesta.compareTo("n") == 0) {
                return false;
            } else {
                System.out.print("Resposta invàlida. Introdueix 'S' o 'N': ");
            }
        }
    }

    public static float leerFloat() {
        while (true) {
            try {
                return Float.parseFloat(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.print("Has ingresado un valor incorrecto. Ingresa un número decimal: ");
            }
        }
    }
}
