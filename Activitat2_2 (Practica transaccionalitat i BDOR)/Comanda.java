package activitat1_2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Comanda {
    int num_comanda;
    float preu_total;
    LocalDate data;
    int dni_client;

    public Comanda() {
    }

    public Comanda(int num_comanda, float preu_total, LocalDate data, int dni_client) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni_client;
    }

    public void imprimir() {
        System.out.println("\nDades de la comanda");
        System.out.println("Número: " + num_comanda);
        System.out.println("Preu: " + preu_total);
        System.out.println("Data: " + data.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }


}
