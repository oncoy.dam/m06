DROP TABLE IF EXISTS RESUM_FACTURACIO;
CREATE TABLE RESUM_FACTURACIO (
	mes int,
	any int,
	dni_client int(8),
	quantitat decimal(10, 2),
	CONSTRAINT PK_RESUM_FACTURACIO PRIMARY KEY (mes, any, dni_client),
	FOREIGN KEY (dni_client) REFERENCES client(DNI)
);

DROP PROCEDURE IF EXISTS crea_resum_facturacio;
DELIMITER //
CREATE PROCEDURE crea_resum_facturacio(IN p_mes int, p_any int)
BEGIN
	DECLARE v_dni int(8);
    DECLARE v_quantitat decimal(10, 2);
    DECLARE v_total decimal(20, 2);
    DECLARE acaba INT DEFAULT FALSE; 
	DECLARE c_clients CURSOR FOR SELECT DISTINCT DNI_client FROM comanda WHERE MONTH(Data) = p_mes AND YEAR(Data) = p_any;
    DECLARE c_comandes CURSOR FOR SELECT Preu_total FROM comanda WHERE DNI_client = v_dni AND MONTH(Data) = p_mes AND YEAR(Data) = p_any;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE; 

    OPEN c_clients;
    read_loop: LOOP
		FETCH c_clients INTO v_dni;
        IF acaba THEN
			LEAVE  read_loop;
		END IF;
        
        SET v_total = 0;
        OPEN c_comandes;
        read2_loop: LOOP
			FETCH c_comandes into v_quantitat;
			IF acaba THEN
				LEAVE read2_loop;
			END IF;
            
            SET v_total = v_total + v_quantitat;
        END LOOP;
        close c_comandes;
        
        INSERT INTO RESUM_FACTURACIO values (p_mes, p_any, v_dni, v_total);
        SET acaba = FALSE;
    END LOOP;
	close c_clients;
END;
