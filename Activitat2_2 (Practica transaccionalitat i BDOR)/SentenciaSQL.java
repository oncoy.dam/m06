package activitat1_2;

import java.io.Closeable;
import java.sql.*;
import java.util.Map;

public class SentenciaSQL implements Closeable {
    Connection con;

    public SentenciaSQL() {
        String url = "jdbc:mysql://localhost/mybbdd";
        String usuari = "myapp";
        String contrasenya = "jupiter";
        try {
            con = DriverManager.getConnection(url, usuari, contrasenya);
            System.out.println("Connexió oberta correctament.");
        } catch (SQLException e) {
            System.out.println("Error al crear la connexió.");
        }
    }

    @Override
    public void close() {
        try {
            con.close();
            System.out.println("Connexió tancada correctament.");
        } catch (SQLException ex) {
            System.err.println("Error al tancar la connexió.");
        }
    }

    public void crearTaules() {
        try {
            java.sql.Statement statement = con.createStatement();
            String sentenciaSQL;

            sentenciaSQL = "CREATE TABLE Client ("
                    + "DNI INT(8) PRIMARY KEY,"
                    + "Nom VARCHAR(100) NOT NULL,"
                    + "Premium ENUM('S','N')"
                    + ")";
            statement.execute(sentenciaSQL);

            sentenciaSQL = "CREATE TABLE Comanda ("
                    + "Num_comanda INT NOT NULL,"
                    + "Preu_total FLOAT(10, 2) NOT NULL,"
                    + "Data DATE,"
                    + "DNI_client INT(8),"
                    + "PRIMARY KEY (Num_comanda),"
                    + "FOREIGN KEY (DNI_client) REFERENCES Client(Dni)"
                    + ")";
            statement.execute(sentenciaSQL);

            System.out.println("Taules creades correctament.");
        } catch (SQLException e) {
            System.out.println("Error al crear les taules.");
        }
    }

    public void emmagatzemar_client(Client client) {

        try {
            String sentenciaSQL = "INSERT INTO Client (DNI, Nom, Premium) values (?, ?, ?);";
            PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
            sentenciaPreparada.setInt(1, client.dni);
            sentenciaPreparada.setString(2, client.nom);
            sentenciaPreparada.setString(3, ((client.premium) ? "S" : "N"));
            sentenciaPreparada.executeUpdate();

        } catch (
                SQLException e) {
            System.out.println("Error al emmagatzemar les dades");
        }
    }

    public void emmagatzemar_comanda(Comanda comanda) {

        try {
            String sentenciaSQL = "INSERT INTO Comanda (Num_comanda, Preu_total, Data, DNI_client) values (?, ?, ?, ?);";
            PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
            sentenciaPreparada.setInt(1, comanda.num_comanda);
            sentenciaPreparada.setFloat(2, comanda.preu_total);
            sentenciaPreparada.setDate(3, Date.valueOf(comanda.data));
            sentenciaPreparada.setInt(4, comanda.dni_client);
            sentenciaPreparada.executeUpdate();

        } catch (
                SQLException e) {
            System.out.println("Error al emmagatzemar les dades");
        }
    }

    public void esborrar(int dni) {
        try {
            Statement statement = con.createStatement();
            String sentenciaSQL = "DELETE FROM Comanda WHERE DNI_client = " + dni;
            statement.execute(sentenciaSQL);
            sentenciaSQL = "DELETE FROM Client WHERE DNI = " + dni;
            statement.execute(sentenciaSQL);
            System.out.println("S'han esborrat les dades correctament.");
        } catch (SQLException e) {
            System.out.println("No s'han pogut esborrar les dades.");
        }


    }

    public void llegir(Map<Integer, Client> clients) {
        clients.clear();
        try {
            Statement statement = con.createStatement();
            Statement statement2 = con.createStatement();

            String sentenciaSQL = "SELECT * FROM Client;";
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            while (rs.next()) {
                Client client = new Client();
                client.dni = rs.getInt("DNI");
                client.nom = rs.getString("Nom");
                client.premium = rs.getString("Premium").equals("S");

                sentenciaSQL = "SELECT * FROM Comanda WHERE DNI_client = " + client.dni + ";";
                ResultSet resultSet = statement2.executeQuery(sentenciaSQL);
                while (resultSet.next()) {
                    Comanda comanda = new Comanda();
                    comanda.num_comanda = resultSet.getInt("Num_comanda");
                    comanda.preu_total = resultSet.getFloat("Preu_total");
                    comanda.data = resultSet.getDate("Data").toLocalDate();
                    comanda.dni_client = resultSet.getInt("DNI_client");
                    client.comandes.put(comanda.num_comanda, comanda);
                }
                clients.put(client.dni, client);
            }
        } catch (SQLException e) {
            System.out.println("Error al llegir les dades de les taules.");
        }
    }

    public void actualizar(int dni, String nom, boolean premium) {
        try {
            String sentenciaSQL = "UPDATE Client SET Nom = ?, Premium = ? WHERE DNI = ?;";
            PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
            sentenciaPreparada.setString(1, nom);
            sentenciaPreparada.setString(2, (premium) ? "S" : "N");
            sentenciaPreparada.setInt(3, dni);
            sentenciaPreparada.executeUpdate();

            System.out.println("S'han actualitzat les dades correctament.");
        } catch (SQLException e) {
            System.out.println("Error al actualitzar les dades.");
        }
    }

    public void mostrar(String principi) {
        try {
            String sentenciaSQL = "SELECT Nom FROM Client WHERE Nom LIKE '" + principi + "%';";
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);
            while (rs.next()) {
                System.out.println(rs.getString("Nom"));
            }
        } catch (SQLException e) {
            System.out.println("Error al mostrar les coincidències");
        }
    }

    public void generar_resum(int mes, int any) {
        String sentenciaSQL = "CALL crea_resum_facturacio(?, ?);";
        PreparedStatement preparedStatement = null;
        try {
            con.setAutoCommit(false);
            preparedStatement = con.prepareStatement(sentenciaSQL);
            preparedStatement.setInt(1, mes);
            preparedStatement.setInt(2, any);
            preparedStatement.executeUpdate();

            con.commit();
            System.out.println("S'ha executat l'ordre correctament.");
        } catch (SQLException e) {
            System.out.println("Error al generar el resum de facturació.");
        } finally {
            try {
                //No he conseguido hacer que el autocommit = true sin que ejecute el preparedStatement en caso de que dé error
                con.setAutoCommit(true);
            } catch (SQLException e) {
                System.out.println("Error en la base de dades.");
            }
        }
    }
}
