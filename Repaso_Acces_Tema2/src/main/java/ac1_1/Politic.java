package ac1_1;

import java.sql.Date;

public class Politic {
    String nif;
    String nom;
    Date data;
    int sou;
    boolean esCorrupte;

    public Politic(String nif, String nom, Date data, int sou, boolean esCorrupte) {
        this.nif = nif;
        this.nom = nom;
        this.data = data;
        this.sou = sou;
        this.esCorrupte = esCorrupte;
    }

    @Override
    public String toString() {
        return "Politic{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", data=" + data +
                ", sou=" + sou +
                ", esCorrupte=" + esCorrupte +
                '}';
    }

    public String getNif() {
        return nif;
    }

    public String getNom() {
        return nom;
    }

    public Date getData() {
        return data;
    }

    public int getSou() {
        return sou;
    }

    public boolean isEsCorrupte() {
        return esCorrupte;
    }
}

