package ac1_1;

import java.io.Closeable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQL implements Closeable{
    private Connection con;

    public SQL() {
        String url = "jdbc:mysql://localhost/mybbdd";
        String user = "myapp";
        String password = "jupiter";
        try {
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Conexión abierta correctamente.");
        } catch (SQLException throwables) {
            System.out.println("Error al crear la conexión.");
        }
    }

    @Override
    public void close() {
        try {
            con.close();
            System.out.println("Conexión cerrada con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al cerrar la conexión.");
        }
    }

    public void crearTabla() throws SQLException {
        String sentencia = "CREATE TABLE IF NOT EXISTS PruebaPolitic(" +
                "nif CHAR(9) PRIMARY KEY," +
                "nom VARCHAR(50)," +
                "dataNaixement DATE," +
                "sou INT(10)," +
                "esCorrupte ENUM('S', 'N')"+
                ");";

        Statement statement = con.createStatement();
        statement.execute(sentencia);
    }

    public void guardarPolitic(Politic politic) throws SQLException {
        String sentencia = "insert into PruebaPolitic values (?,?,?,?,?);";

        PreparedStatement preparedStatement = con.prepareStatement(sentencia);
        preparedStatement.setString(1, politic.nif);
        preparedStatement.setString(2, politic.nom);
        preparedStatement.setDate(3, politic.data);
        preparedStatement.setInt(4, politic.sou);
        preparedStatement.setString(5, politic.esCorrupte ? "S" : "N");

        preparedStatement.executeUpdate();

        System.out.println("Se ha guardado el político.");
    }

    public void leerPolitic() throws SQLException{
        Statement statement = con.createStatement();
        String sentencia = "SELECT * FROM PruebaPolitic;";
        ResultSet rs = statement.executeQuery(sentencia);

        List<Politic> politics = new ArrayList<>();
        while (rs.next()) {
            Politic politic = new Politic(rs.getString("nif"), rs.getString("nom"), rs.getDate("dataNaixement"), rs.getInt("sou"), rs.getString("esCorrupte").equals("S"));
            politics.add(politic);
        }

        for (Politic politic : politics)
            System.out.println(politic);
    }
}
