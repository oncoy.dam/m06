package ac1_1;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {

        try (SQL sql = new SQL()){
            sql.crearTabla();
            Politic politic = new Politic("12142180d", "Bryan", Date.valueOf("2020-10-04"), 1234, true);
            sql.guardarPolitic(politic);
            sql.leerPolitic();
        } catch (SQLException e) {
            System.out.println("Error en el procedimiento.");
        }
    }
}
