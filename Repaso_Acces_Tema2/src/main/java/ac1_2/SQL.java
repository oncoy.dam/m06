package ac1_2;

import util.Teclado;

import javax.swing.plaf.nimbus.State;
import java.io.Closeable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQL implements Closeable {
    private Connection con;

    public SQL() {
        String url = "jdbc:mysql://localhost/mybbdd";
        String user = "myapp";
        String password = "jupiter";
        try {
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Conexión abierta correctamente.");
        } catch (SQLException throwables) {
            System.out.println("Error al crear la conexión.");
        }
    }

    @Override
    public void close() {
        try {
            con.close();
            System.out.println("Conexión cerrada con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al cerrar la conexión.");
        }
    }

    public void crearTablas() {
        String sentencia = "CREATE TABLE PruebaClient(" +
                "dni INT(8)," +
                "nom VARCHAR(50)," +
                "premium ENUM('S', 'N')," +
                "PRIMARY KEY (dni)" +
                ");";

        try {
            Statement statement = con.createStatement();
            statement.execute(sentencia);
            System.out.println("Se ha creado la tabla PruebaClient.");
        } catch (SQLException e) {
            System.out.println("Error al crear la tabla PruebaClient");
        }

        sentencia = "CREATE TABLE PruebaComanda(" +
                "num_comanda INT(10)," +
                "preu_total FLOAT(10, 2)," +
                "data DATE," +
                "dni_client INT(8)," +
                "PRIMARY KEY (num_comanda)," +
                "FOREIGN KEY (dni_client) REFERENCES PruebaClient(dni)" +
                ");";

        try {
            Statement statement = con.createStatement();
            statement.execute(sentencia);
            System.out.println("Se ha careado la tabla PruebaClient.");
        } catch (SQLException e) {
            System.out.println("Error al crear la tabla PruebaComanda.");
        }
    }

    public List<Client> recuperarDades() {
        List<Client> clients = new ArrayList<>();

        try (Statement statement = con.createStatement();
             Statement statement2 = con.createStatement()) {

            String sentencia = "SELECT * FROM PruebaClient;";
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                Client client = new Client(rs.getInt("dni"), rs.getString("nom"), rs.getString("premium").equals("S"));

                sentencia = "SELECT * FROM PruebaComanda WHERE dni_client = " + client.dni + ";";
                ResultSet rs2 = statement2.executeQuery(sentencia);
                while (rs2.next()) {
                    Comanda comanda = new Comanda(rs2.getInt("num_comanda"), rs2.getFloat("preu_total"), rs2.getDate("data"), rs2.getInt("dni_client"));
                    client.comandes.add(comanda);
                }
                clients.add(client);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar los datos.");
        }

        return clients;
    }

    public void emmagatzemarDades(List<Client> clients) {
        String sentencia = "DELETE FROM PruebaComanda;";
        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);

            sentencia = "DELETE FROM PruebaClient;";
            statement.execute(sentencia);

            for (Client client : clients) {
                sentencia = "INSERT INTO PruebaClient VALUES (?, ?, ?);";
                PreparedStatement preparedStatement = con.prepareStatement(sentencia);

                preparedStatement.setInt(1, client.dni);
                preparedStatement.setString(2, client.nom);
                preparedStatement.setString(3, client.premium ? "S" : "N");

                preparedStatement.executeUpdate();

                for (Comanda comanda : client.comandes) {
                    sentencia = "INSERT INTO PruebaComanda VALUES (?, ?, ?, ?);";
                    preparedStatement = con.prepareStatement(sentencia);

                    preparedStatement.setInt(1, comanda.num_comanda);
                    preparedStatement.setFloat(2, comanda.preu_total);
                    preparedStatement.setDate(3, comanda.data);
                    preparedStatement.setInt(4, comanda.dni_client);

                    preparedStatement.executeUpdate();
                }
            }
            System.out.println("Se han guardado los datos con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al almacenar los datos.");
        }
    }

    public void insertarCliente(List<Client> clients) {
        System.out.println("Introduzca los datos del cliente.");
        int dni;
        boolean dni_valido;
        do {
            dni_valido = true;
            System.out.print("DNI: ");
            dni = Teclado.leerInt();
            for (Client client : clients) {
                if (client.dni == dni) {
                    dni_valido = false;
                    System.out.println("Este DNI ya está en uso.");
                    break;
                }
            }
        } while (!dni_valido);

        clients.add(new Client(dni, "Peter", true));
        String sentencia = "INSERT INTO PruebaClient VALUES(?, ?, ?)";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sentencia);
            preparedStatement.setInt(1, dni);
            preparedStatement.setString(2, "Peter");
            preparedStatement.setString(3, "N");
            preparedStatement.executeUpdate();
            System.out.println("Se ha guardado el cliente.");
        } catch (SQLException e) {
            System.out.println("Error al guardar el cliente.");
        }
    }

    public void insertarComanda(List<Client> clients) {
        System.out.println("Seleccione el cliente: ");
        int contador = 1;
        for (Client client : clients) {
            System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            contador++;
        }
        int opcio = Teclado.leerInt(1, clients.size()) - 1;

        int dni_client = clients.get(opcio).dni;

        System.out.print("Introduzca el número de comanda: ");
        boolean num_valido;
        int num_comanda;
        do {
            num_valido = true;
            num_comanda = Teclado.leerInt();
            for (Client client : clients) {
                for (Comanda comanda : client.comandes) {
                    if (num_comanda == comanda.num_comanda) {
                        System.out.println("Este número ya está registrado.");
                        num_valido = false;
                        break;
                    }
                }
                if (!num_valido)
                    break;
            }
        } while (!num_valido);

        clients.get(opcio).comandes.add(new Comanda(num_comanda, 14.50f, Date.valueOf("2021-01-13"), dni_client));

        String sentencia = "INSERT INTO PruebaComanda VALUES (?, ?, ?, ?);";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sentencia);
            preparedStatement.setInt(1, num_comanda);
            preparedStatement.setFloat(2, 14.50f);
            preparedStatement.setDate(3, Date.valueOf("2021-01-13"));
            preparedStatement.setInt(4, dni_client);
            preparedStatement.executeUpdate();
            System.out.println("Se ha guardado la comanda.");
        } catch (SQLException e) {
            System.out.println("Error al guardar la comanda.");
        }
    }

    public void eliminarClient(List<Client> clients) {
        System.out.println("Seleccione el cliente: ");
        int contador = 1;
        for (Client client : clients) {
            System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            contador++;
        }
        int opcio = Teclado.leerInt(1, clients.size()) - 1;

        Client client = clients.get(opcio);

        String sentencia = "DELETE FROM PruebaComanda WHERE dni_client = " + client.dni + ";";
        String sentencia2 = "DELETE FROM PruebaClient WHERE dni = " + client.dni + ";";
        try (Statement statement = con.createStatement()) {
            statement.execute(sentencia);
            statement.execute(sentencia2);
            clients.remove(opcio);
            System.out.println("Se ha eliminado al cliente y todas sus comandas.");
        } catch (SQLException e) {
            System.out.println("Error al borrar el cliente");
        }
    }

    public void actualitzarClient(List<Client> clients) {
        System.out.println("Seleccione el cliente: ");
        int contador = 1;
        for (Client client : clients) {
            System.out.printf("%d. %d - %s\n", contador, client.dni, client.nom);
            contador++;
        }
        int opcio = Teclado.leerInt(1, clients.size()) - 1;
        Client client = clients.get(opcio);
        String sentencia = "UPDATE PruebaClient SET nom = ?, premium = ? WHERE dni = " + client.dni + ";";
        try (PreparedStatement preparedStatement = con.prepareStatement(sentencia)) {
            preparedStatement.setString(1, "Liling");
            preparedStatement.setString(2, "S");
            preparedStatement.executeUpdate();
            client.nom = "Liling";
            client.premium = true;
            System.out.println("Se ha actualizado el cliente");
        } catch (SQLException e) {
            System.out.println("Error al actualizar");
        }
    }

    public void mostrarPorNombre() {
        System.out.print("Indique el comienzo del nombre a buscar: ");
        String patron = Teclado.leer();
        String sentencia = "SELECT nom FROM PruebaClient WHERE nom LIKE '" + patron + "%';";
        try (Statement statement = con.createStatement()) {
            ResultSet rs = statement.executeQuery(sentencia);
            while (rs.next()) {
                System.out.println(rs.getString("nom"));
            }
        } catch (SQLException e) {
            System.out.println("Error al buscar el nombre.");
        }
    }
}
