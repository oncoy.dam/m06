package ac1_2;

import java.util.ArrayList;
import java.util.List;

public class Client {
    int dni;
    String nom;
    boolean premium;
    List<Comanda> comandes;

    public Client() {
        comandes = new ArrayList<>();
    }

    public Client(int dni, String nom, boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
        this.comandes = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Client{" +
                "dni=" + dni +
                ", nom='" + nom + '\'' +
                ", premium=" + premium +
                ", comandes=" + comandes +
                '}';
    }
}
