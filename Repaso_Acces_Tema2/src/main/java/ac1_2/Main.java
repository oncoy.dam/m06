package ac1_2;

import util.Teclado;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try (SQL sql = new SQL()) {
            List<Client> clients = sql.recuperarDades();
            while (true) {
                System.out.println("Menú");
                switch (Teclado.leerInt(1, 5)) {
                    case 1:
                        sql.eliminarClient(clients);
                        break;
                    case 2:
                        sql.actualitzarClient(clients);
                        break;
                    case 3:
                        sql.mostrarPorNombre();
                        break;
                    case 4:
                        sql.insertarCliente(clients);
                        break;
                    case 5:
                        sql.insertarComanda(clients);
                }
            }
        }
    }
}
