package ac1_2;

import java.sql.Date;

public class Comanda {
    int num_comanda;
    float preu_total;
    Date data;
    int dni_client;

    public Comanda(int num_comanda, float preu_total, Date data, int dni_client) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni_client;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "num_comanda=" + num_comanda +
                ", preu_total=" + preu_total +
                ", data=" + data +
                ", dni_client=" + dni_client +
                '}';
    }
}

