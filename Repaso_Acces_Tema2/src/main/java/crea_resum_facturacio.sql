CREATE DEFINER=`myapp`@`%` PROCEDURE `crea_resum_facturacio`(IN p_mes int, p_any int)
BEGIN
	DECLARE v_dni int(8);
    DECLARE v_total decimal(20, 2);
    DECLARE acaba INT DEFAULT FALSE;
	DECLARE c_preutotal CURSOR FOR SELECT DNI_client, SUM(Preu_total) FROM Comanda WHERE DNI_Client IN (SELECT Distinct DNI_Client FROM Comanda)
    and MONTH(Data) = p_mes and YEAR(Data) = p_any GROUP BY Dni_client;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE;

	CREATE TABLE IF NOT EXISTS RESUM_FACTURACIO (
	mes int,
	any int,
	dni_client int(8),
	quantitat decimal(10, 2),
	CONSTRAINT PK_RESUM_FACTURACIO PRIMARY KEY (mes, any, dni_client),
	FOREIGN KEY (dni_client) REFERENCES client(DNI)
	);

    DELETE FROM RESUM_FACTURACIO WHERE mes = p_mes and any = p_any;

    OPEN c_preutotal;
    read_loop: LOOP
		FETCH c_preutotal INTO v_dni, v_total;
        IF acaba THEN
			LEAVE  read_loop;
		END IF;

        INSERT INTO RESUM_FACTURACIO values (p_mes, p_any, v_dni, v_total);
        SET acaba = FALSE;
    END LOOP;
	close c_preutotal;
END