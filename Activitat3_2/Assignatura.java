package Activitat3_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Assignatura {
    private int numero;
    private String nom;
    private int durada;
    private List<Alumne> alumnes;

    public Assignatura() {
        alumnes = new ArrayList<>();
    }

    public Assignatura(int numero, String nom, int durada) {
        this.numero = numero;
        this.nom = nom;
        this.durada = durada;
        alumnes = new ArrayList<>();
    }

    public List<Alumne> getAlumnes() {
        return alumnes;
    }

    public void imprimir() {
        System.out.println("\nAssignatura número: " + numero);
        System.out.println("Nom: " + nom);
        System.out.println("Durada: " + durada);
        System.out.println("Llista d'alumnes: ");
        System.out.println("--------------------");
        for (Alumne alumne : alumnes) {
            alumne.imprimir();
            System.out.println("--------------------");
        }
    }

    public void afegirAlumnes() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introdueixi el nom de l'alumne: ");
        String nom = sc.nextLine();
        System.out.print("Introdueixi el DNI de l'alumne: ");
        String dni = sc.nextLine();
        System.out.print("És repetidor?\n1. Sí \n2. No\n");
        int rep = Util.opcio(1, 2);
        boolean repetidor;
        repetidor = rep == 1;
        alumnes.add(new Alumne(nom, dni, repetidor));
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDurada() {
        return durada;
    }

    public void setDurada(int durada) {
        this.durada = durada;
    }

}
