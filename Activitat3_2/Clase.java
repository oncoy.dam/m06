package Activitat3_2;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Clase {

    public static void main(String[] args) {
        List<Assignatura> listAssignatura = new ArrayList<>();
        boolean continuar = true;
        while (continuar) {

            Util.menu();
            int opcio = Util.opcio(0, 6);
            switch (opcio) {
                case 0:
                    continuar = false;
                    break;
                case 1:
                    listAssignatura = cas1();
                    break;
                case 2:
                    listAssignatura = cas2();
                    break;
                case 3:
                    cas3(listAssignatura);
                    break;
                case 4:
                    cas4(listAssignatura);
                    break;
                case 5:
                    cas5(listAssignatura);
                    break;
                case 6:
                    cas6(listAssignatura);
                    break;
            }
            if (opcio != 0) {
                System.out.println("\nDesitja fer una altra operació? \n1.Sí \n2.No");
                if (Util.opcio(1, 2) == 2) {
                    continuar = false;
                }
            }

            if (!continuar) {
                System.out.println("El programa ha finalitzat.");
            }
        }
    }

    public static List<Assignatura> cas1() {
        Scanner sc = new Scanner(System.in);
        List<Assignatura> listAssignatura;

        while (true) {
            //Pediremos el nombre del fichero hasta que se introduzca uno que exista.
            System.out.print("Introdueixi el nom del fitxer a llegir fitxer.xml: ");
            String nom = sc.nextLine();
            try {
                listAssignatura = Util.sequencial(nom);
                System.out.println("S'han recuperat les dades del fitxer " + nom + " correctament.");
                break;
            } catch (ParserConfigurationException | IOException | SAXException e) {
                System.out.println("El fitxer no existeix.");
            }
        }
        return listAssignatura;
    }

    public static List<Assignatura> cas2() {
        Scanner sc = new Scanner(System.in);
        List<Assignatura> listAssignatura;

        while (true) {
            System.out.print("Introdueixi el nom del fitxer a llegir fitxer.xml: ");
            String nom = sc.nextLine();
            try {
                listAssignatura = Util.sintactic(nom);
                System.out.println("S'han recuperat les dades del fitxer " + nom + " correctament.");
                break;
            } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
                System.out.println("El fitxer no existeix.");
            }
        }

        return listAssignatura;
    }

    public static void cas3(List<Assignatura> listAssignatura) {
        if (listAssignatura != null) {
            for (Assignatura assignatura : listAssignatura) {
                assignatura.imprimir();
            }
        } else {
            System.out.println("No té una llista per poder imprimirla.");
        }
    }

    public static void cas4(List<Assignatura> listAssignatura) {
        Scanner sc = new Scanner(System.in);
        int numero;
        boolean repetido;
        do {
            repetido = false;
            System.out.print("Ingresi el número de l'assignatura: ");
            numero = Util.entero();
            if (!listAssignatura.isEmpty()) {
                for (Assignatura assignatura : listAssignatura) {
                    if (numero == assignatura.getNumero()) {
                        System.out.println("Aquest número d'assignatura ja està registrat.");
                        repetido = true;
                        break;
                    }
                }
            }
        } while (repetido);

        System.out.print("Ingresi el nom de l'assignatura: ");
        String nom = sc.nextLine();
        System.out.print("Ingresi la durada de l'assignatura: ");
        int durada = Util.entero();
        listAssignatura.add(new Assignatura(numero, nom, durada));
    }

    public static void cas5(List<Assignatura> listAssignatura) {
        if (listAssignatura.isEmpty()) {
            System.out.println("No tens cap assignatura registrada.");
        } else {
            int numAssignatura;
            boolean continuar = true;
            int index = 0;
            while (continuar) {
                System.out.print("Introdueixi el número de l'assignatura on vol registrar l'alumne: ");
                numAssignatura = Util.entero();
                for (Assignatura assignatura : listAssignatura) {
                    if (assignatura.getNumero() == numAssignatura) {
                        index = listAssignatura.indexOf(assignatura);
                        continuar = false;
                        break;
                    }
                }
                if (continuar)
                    System.out.println("Aquesta assignatura no existeix.");
            }
            listAssignatura.get(index).afegirAlumnes();
        }
    }

    public static void cas6(List<Assignatura> listAssignatura) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introdueixi el nom del fitxer on vol desar les dades: ");
        String ruta = sc.nextLine();
        try {
            Util.crearXML(listAssignatura, ruta);
            System.out.println("S'han desat les dades correctament.");
        } catch (ParserConfigurationException | TransformerException e) {
            System.out.println("Error al desar les dades.");
        }

    }
}
