package Activitat3_2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Util {
    public static void menu() {
        System.out.println("\n" +
                "0 - Sortir \n" +
                "1 - Llegir d'un fitxer XML pel mètode seqüencial \n" +
                "\t* Es demanarà el nom del fitxer \n" +
                "2 - Llegir d'un fitxer XML pel mètode sintàctic \n" +
                "\t* Es demanarà el nom del fitxer \n" +
                "3 - Mostrar per pantalla totes les assignatures amb les seves dades \n" +
                "\t(número, nom, durada i la llista d'alumnes) \n" +
                "4 - Afegir una assignatura \n" +
                "\t* Es demanaran les dades bàsiques (número, nom i durada) \n" +
                "5 - Afegir un alumne a una assignatura \n" +
                "\t* Es demanarà el número de l'assignatura (caldrà comprovar si existeix l'assignatura) \n" +
                "\t* Es demanaran les de l'alumne (nom, dni i si és repetidor) \n" +
                "\t* S'afegirà a la llista d'alumnes de l'assignatura \n" +
                "6 - Guardar a disc en XML amb les assignatures \n");
    }

    public static List<Assignatura> sequencial(String nomFitxer) throws ParserConfigurationException, IOException, SAXException, NumberFormatException {
        Document doc =
                DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(nomFitxer);
        //Creamos un nodo con el elemento raíz "assignaturas" y una lista de sus elementos
        Node node_Assignatures = doc.getDocumentElement();
        NodeList nodeList_Assignatures = node_Assignatures.getChildNodes();
        List<Assignatura> assignatures = new ArrayList<>();

        for (int i = 0; i < nodeList_Assignatures.getLength(); i++) {
            if (nodeList_Assignatures.item(i).getNodeName().equals("assignatura")) {
                //Creamos un objeto de tipo Assignatura para guardar todos los datos obtenidos en él
                Assignatura assignatura = new Assignatura();
                //Creamos una nodeList con el contenido del tag "assignatura" y la recorremos
                NodeList nodeList_Assignatura = nodeList_Assignatures.item(i).getChildNodes();
                for (int j = 0; j < nodeList_Assignatura.getLength(); j++) {
                    Node assigActual = nodeList_Assignatura.item(j);
                    //Solo trabajamos con los nodos de tag "numero", "nom", "durada" o "alumnes"
                    switch (assigActual.getNodeName()) {
                        case "numero":
                            assignatura.setNumero(Integer.parseInt(assigActual.getTextContent()));
                            break;
                        case "nom":
                            assignatura.setNom(assigActual.getTextContent());
                            break;
                        case "durada":
                            assignatura.setDurada(Integer.parseInt(assigActual.getTextContent()));
                            break;
                        case "alumnes":
                            //Creamos una nodeList con el contenido del tag "alumnes" y la recorremos
                            NodeList nodeList_Alumnes = assigActual.getChildNodes();
                            for (int k = 0; k < nodeList_Alumnes.getLength(); k++) {
                                //Solo trabajamos con los nodos de tag "alumne"
                                if ((nodeList_Alumnes.item(k).getNodeName()).equals("alumne")) {
                                    Alumne alumne = new Alumne();
                                    //Creamos una nodeList con el contenido del tag "alumne" y la recorremos
                                    NodeList nodeList_Alumne = nodeList_Alumnes.item(k).getChildNodes();
                                    for (int l = 0; l < nodeList_Alumne.getLength(); l++) {
                                        //Solo trabajamos con los nodos de tag "nom", "dni" o "repetidor"
                                        Node alumActual = nodeList_Alumne.item(l);
                                        switch (alumActual.getNodeName()) {
                                            case "nom":
                                                alumne.setNom(alumActual.getTextContent());
                                                break;
                                            case "dni":
                                                alumne.setDni(alumActual.getTextContent());
                                                break;
                                            case "repetidor":
                                                alumne.setRepetidor(Boolean.parseBoolean(alumActual.getTextContent()));
                                                break;
                                        }

                                    }
                                    //Añadimos el Alumne a la List<Alumne> de Assignatura
                                    assignatura.getAlumnes().add(alumne);
                                }
                            }
                            break;
                    }
                }
                //Añadimos el objeto Assignatura con todos sus datos a la List<Assignatura>
                assignatures.add(assignatura);
            }
        }
        return assignatures;
    }

    public static List<Assignatura> sintactic(String nomFitxer) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        List<Assignatura> assignatures = new ArrayList<>();
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(nomFitxer);

        //Creamos y recorremos un NodeList que contiene todos los elementos con el tag <assignatura>
        NodeList nodeList_Assignatures = tornarNodeList(doc, "/assignatures/assignatura");

        for (int i = 0; i < nodeList_Assignatures.getLength(); i++) {
            //En cada iteración obtenemos el elemento <assignatura> en cuestión
            Node node_Assignatura = nodeList_Assignatures.item(i);

            //Obtenemos los valores de los diferentes tags que hay dentro del Node donde nos encontramos (node_Assignatura)
            String num = tornarNodeList(node_Assignatura, "numero").item(0).getTextContent();
            String nom = tornarNodeList(node_Assignatura, "nom").item(0).getTextContent();
            String durada = tornarNodeList(node_Assignatura, "durada").item(0).getTextContent();
            Assignatura assignatura = new Assignatura(Integer.parseInt(num), nom, Integer.parseInt(durada));

            //Creamos y recorremos un NodeList que contiene todos los elementos <alumne> de la <assignatura> en la que nos encontramos
            NodeList nodeList_Alumne = tornarNodeList(node_Assignatura, "alumnes/alumne");
            for (int j = 0; j < nodeList_Alumne.getLength(); j++) {
                Node node_Alumne = nodeList_Alumne.item(j);

                //Obtenemos los valores de los diferentes tags que hay dentro del Node donde nos encontramos (node_Alumne)
                String nomAlumne = tornarNodeList(node_Alumne, "nom").item(0).getTextContent();
                String dni = tornarNodeList(node_Alumne, "dni").item(0).getTextContent();
                String repetidor = tornarNodeList(node_Alumne, "repetidor").item(0).getTextContent();

                assignatura.getAlumnes().add(new Alumne(nomAlumne, dni, Boolean.parseBoolean(repetidor)));
            }
            assignatures.add(assignatura);
        }
        return assignatures;
    }

    //Método que devuelve un NodeList con los elementos <tag> del Objeto pasado como argumento (ya sea Document o Node)
    public static NodeList tornarNodeList(Object obj, String tag) throws XPathExpressionException {
        XPathExpression expr = XPathFactory.newInstance().newXPath().compile(tag);
        return (NodeList) expr.evaluate(obj, XPathConstants.NODESET);
    }

    public static void crearXML(List<Assignatura> listAssignatura, String ruta) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        //Creamos la raíz assignatures
        Element assignatures = document.createElement("assignatures");
        document.appendChild(assignatures);

        for (Assignatura assignatura : listAssignatura) {
            Element element_assignatura = document.createElement("assignatura");
            assignatures.appendChild(element_assignatura);

            Element numero = document.createElement("numero");
            numero.setTextContent(String.valueOf(assignatura.getNumero()));
            element_assignatura.appendChild(numero);

            Element nom = document.createElement("nom");
            nom.setTextContent(assignatura.getNom());
            element_assignatura.appendChild(nom);

            Element durada = document.createElement("durada");
            durada.setTextContent(String.valueOf(assignatura.getDurada()));
            element_assignatura.appendChild(durada);

            Element alumnes = document.createElement("alumnes");
            element_assignatura.appendChild(alumnes);

            for (Alumne alumne : assignatura.getAlumnes()) {
                Element element_alumne = document.createElement("alumne");
                alumnes.appendChild(element_alumne);

                Element nom_alumne = document.createElement("nom");
                nom_alumne.setTextContent(alumne.getNom());
                element_alumne.appendChild(nom_alumne);

                Element dni = document.createElement("dni");
                dni.setTextContent(alumne.getDni());
                element_alumne.appendChild(dni);

                Element repetidor = document.createElement("repetidor");
                repetidor.setTextContent(String.valueOf(alumne.isRepetidor()));
                element_alumne.appendChild(repetidor);
            }
        }

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(ruta));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, result);
    }

    public static int entero() {
        Scanner sc = new Scanner(System.in);
        int opcio;
        //Hacemos un bucle que solo parará cuando el número sea una opción válida
        while (true) {
            String resposta = sc.nextLine();
            try {
                opcio = Integer.parseInt(resposta);
                break;
            } catch (NumberFormatException e) {
                System.out.print("Introdueixi un número enter: ");
            }
        }
        return opcio;
    }

    public static int opcio(int min, int max) {
        Scanner sc = new Scanner(System.in);
        int opcio;
        //Hacemos un bucle que solo parará cuando el número sea una opción válida
        while (true) {
            System.out.print("Seleccioni una opció: ");
            String resposta = sc.nextLine();
            try {
                opcio = Integer.parseInt(resposta);
                if (opcio >= min && opcio <= max) {
                    break;
                }
            } catch (NumberFormatException e) {
                //El mensaje lo mostramos al final
            }
            System.out.printf("Introdueixi una opció vàlida (%d-%d)\n", min, max);
        }
        return opcio;
    }
}
