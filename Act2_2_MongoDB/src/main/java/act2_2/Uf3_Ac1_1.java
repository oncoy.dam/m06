/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package act2_2;

import java.util.List;

/**
 *
 * @author Jordi
 */
public class Uf3_Ac1_1 {

    public static void exercici1() {
        Assignatura a1 = new Assignatura("Accés a dades",5);
        a1.alumnes_matriculats.add(new Alumne("12345678", "Bryan"));
        a1.alumnes_matriculats.add(new Alumne("12345679", "Liling"));
        a1.alumnes_matriculats.add(new Alumne("23456789", "Peter"));
        AssignaturaMongo.insereix(a1);

        Assignatura a2 = new Assignatura("Sistemes de gestió empresarial",3);
        a2.alumnes_matriculats.add(new Alumne("12345678", "Byron"));
        a2.alumnes_matriculats.add(new Alumne("12345679", "Lulung"));
        a2.alumnes_matriculats.add(new Alumne("23456789", "Putor"));
        a2.alumnes_matriculats.add(new Alumne("12345678", "Byron"));
        a2.alumnes_matriculats.add(new Alumne("12345679", "Lulung"));
        a2.alumnes_matriculats.add(new Alumne("23456789", "Putor"));
        AssignaturaMongo.insereix(a2);
    }
    
    public static void exercici2() {
        List<Assignatura> llistaAssig = AssignaturaMongo.llegeix(5);
        for (Assignatura assig : llistaAssig) {
            System.out.println(assig);
        }        
    }
    
    public static void exercici3() {
        AssignaturaMongo.elimina();
    }
    
    public static void main(String[] args) {
        exercici3();
    }
    
}
