package act2_2;

public class Alumne {
    String dni;
    String nom;

    public Alumne(String dni, String nom) {
        this.dni = dni;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "\n\tAlumne {dni: " + dni + ", nom: " + nom + "}";
    }
}
