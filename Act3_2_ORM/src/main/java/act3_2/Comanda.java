package act3_2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Entity
@Table (name = "comanda")
public class Comanda {
    @Id
    @Column (name = "Num_comanda")
    private int num_comanda;

    @Column (name = "Preu_total")
    private float preu_total;

    @Column (name = "Data")
    private Date data;

    @Column (name = "DNI_client")
    private int dni_client;

    public Comanda() {
    }

    public Comanda(int num_comanda, float preu_total, Date data, int dni_client) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni_client;
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return "\nComanda:" +
                "\nnum_comanda = " + num_comanda +
                "\npreu_total = " + preu_total +
                "\ndata = " + dateFormat.format(data) +
                "\ndni_client = " + dni_client;
    }
}
