package act3_2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table (name = "client")
public class Client {
    @Id
    @Column (name = "DNI")
    private int dni;

    @Column (name = "Nom")
    private String nom;

    //He posat premium com a String perquè a la base de dades ho tinc com enum i no me deixava desar el client si tenia premium com a boolean.
    @Column (name = "Premium")
    String premium;

    public Client() {
    }

    public Client(int dni, String nom, String premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }
}
