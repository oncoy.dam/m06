package act3_2;

public class Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("\n0.- Sortir.\n" +
                    "1.- Eliminació d'un client.\n" +
                    "2.- Actualització de les dades d'un client.\n" +
                    "3.- Cerca per nom de client\n" +
                    "4.- Alta d'un nou client\n" +
                    "5.- Alta d'una nova comanda\n" +
                    "6.- Mostrar per pantalla les comandes d'un client\n" +
                    "7. Mostrar per pantalla un resum de tots els clients amb la quantitat total que han facturat, ordenat de més facturació a menys facturació.");
            System.out.print("Opció: ");
            int opcion = Teclado.leerInt(1, 7);

            switch (opcion) {
                case 0:
                    break;
                case 1:
                    ORM.eliminarClient();
                    break;
                case 2:
                    ORM.actualitzarDades();
                    break;
                case 3:
                    ORM.cercarClient();
                    break;
                case 4:
                    ORM.registrarClient();
                    break;
                case 5:
                    ORM.registrarComanda();
                    break;
                case 6:
                    ORM.mostrarComandes();
                    break;
                case 7:
                    ORM.mostrarResum();
                    break;
            }
        }
    }
}
