package act3_2;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Teclado {
    private static Scanner sc = new Scanner(System.in);
    private static boolean error;

    public static String leerStr() {
        return sc.nextLine();
    }

    //Método para leer un entero filtrando errores
    public static int leerInt() {
        while (true) {
            try {
                return Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.print("Ha introduït un valor incorrecte. Introdueixi un número enter: ");
            }
        }
    }

    //Método para leer un entero entre un rango de números
    public static int leerInt(int min, int max) {
        int valor;
        while (true) {
            valor = leerInt();
            if (valor < min || valor > max) {
                System.out.println("Opció escollida no vàlida.");
                System.out.print("Introdueixi un valor entre " + min + " i " + max + ": ");
            } else {
                return valor;
            }
        }
    }

    //Método para leer un S/N
    public static boolean leerSN() {
        String respuesta;
        while (true) {
            respuesta = sc.nextLine().toLowerCase();
            if (respuesta.compareTo("s") == 0) {
                return true;
            } else if (respuesta.compareTo("n") == 0) {
                return false;
            } else {
                System.out.print("Resposta invàlida. Introdueixi 'S' o 'N': ");
            }
        }
    }

    public static float leerFloat() {
        while (true) {
            try {
                return Float.parseFloat(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.print("Ha introduït un valor incorrecte. Introdueixi un número decimal: ");
            }
        }
    }

    public static Date leerDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        while (true) {
            String data = sc.nextLine();
            try {
                return Date.valueOf(LocalDate.parse(data, formatter));
            } catch (DateTimeParseException e) {
                System.out.print("Ha introduït una data incorrecta. Introdueixi una data (dd/MM/yyyy): ");
            }
        }
    }
}
