package act3_2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.sql.Date;
import java.util.*;

public class ORM {

    public static void eliminarClient() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("from Client");
            List<Client> clients = query.list();

            if (clients.isEmpty()) {
                System.out.println("No hi ha clients registrats.");
            } else {
                System.out.println("Seleccioni un client per eliminar:");
                int contador = 1;
                for (Client client : clients) {
                    System.out.printf("%d. %d - %s\n", contador, client.getDni(), client.getNom());
                    contador++;
                }
                int opcio = Teclado.leerInt(1, clients.size()) - 1;
                Client client = clients.get(opcio);

                query = session.createQuery("FROM Comanda WHERE dni_client = " + client.getDni());
                List<Comanda> comandes = query.list();
                for (Comanda comanda : comandes)
                    session.delete(comanda);

                session.delete(client);
                session.getTransaction().commit();
            }
        }
    }


    public static void actualitzarDades() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("from Client");
            List<Client> clients = query.list();
            if (clients.isEmpty()) {
                System.out.println("No hi ha cap client registrat.");
            } else {
                System.out.println("Aquests són els clients:");
                int contador = 1;
                for (Client client : clients) {
                    System.out.printf("%d. %d - %s\n", contador, client.getDni(), client.getNom());
                    contador++;
                }

                System.out.print("Seleccioni el client per actualitzar les seves dades: ");
                int opcio = Teclado.leerInt(1, clients.size()) - 1;
                Client client = clients.get(opcio);

                System.out.print("Introdueixi el nou nom del client: ");
                client.setNom(Teclado.leerStr());

                System.out.print("Introdueixi si és premium: ");
                client.setPremium(Teclado.leerSN() ? "S" : "N");

                session.update(client);
                session.getTransaction().commit();
            }
        }
    }


    public static void cercarClient() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            System.out.println("Introdueixi el començament del nom que vulgueu buscar");
            String nom = Teclado.leerStr();
            Query query = session.createQuery("from Client where Nom Like '" + nom + "%'");
            List<Client> clients = query.list();
            if (clients.isEmpty()) {
                System.out.println("No hi ha cap client que comenci d'aquesta manera.");
            } else {
                System.out.println("S'han trobat els següents clients:");
                for (Client client : clients) {
                    System.out.printf("%s amb DNI: %d\n", client.getNom(), client.getDni());
                }
            }
            session.getTransaction().commit();
        }
    }


    public static void registrarClient() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("SELECT C.dni FROM Client C");
            List<Integer> lista_dni = query.list();

            int dni;
            while (true) {
                System.out.print("Introdueixi el DNI del client: ");
                dni = Teclado.leerInt();
                if (lista_dni.contains(dni))
                    System.out.println("Aquest DNI ja està registrat.");
                else
                    break;
            }
            System.out.print("Introdueixi el nom del client: ");
            String nom = Teclado.leerStr();
            System.out.print("Es premium? (S/N): ");
            String premium = Teclado.leerSN() ? "S" : "N";
            Client client = new Client(dni, nom, premium);

            session.save(client);
            session.getTransaction().commit();
        }
    }


    public static void registrarComanda() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("from Client");
            List<Client> clients = query.list();
            if (clients.isEmpty()) {
                System.out.println("No hi ha cap client registrat.");
            } else {
                System.out.println("Aquests són els clients:");
                int contador = 1;
                for (Client client : clients) {
                    System.out.printf("%d. %d - %s\n", contador, client.getDni(), client.getNom());
                    contador++;
                }
                System.out.print("Seleccioni el client de la comanda: ");
                int opcio = Teclado.leerInt(1, clients.size()) - 1;
                int dniClient = clients.get(opcio).getDni();

                query = session.createQuery("SELECT C.num_comanda FROM Comanda C");
                List<Integer> nums_comandes = query.list();

                int numComanda;
                while (true) {
                    System.out.print("Introdueixi el número de la comanda: ");
                    numComanda = Teclado.leerInt();
                    if (nums_comandes.contains(numComanda))
                        System.out.println("Aquest número de comanda ja està registrat.");
                    else
                        break;
                }

                System.out.print("Introdueixi el preu de la comanda: ");
                float preu = Teclado.leerFloat();

                System.out.print("Introdueixi la data de la comanda: ");
                Date data = Teclado.leerDate();

                Comanda comanda = new Comanda(numComanda, preu, data, dniClient);
                session.save(comanda);
                session.getTransaction().commit();
            }
        }
    }


    public static void mostrarComandes() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from Client");
            List<Client> clients = query.list();
            if (clients.isEmpty()) {
                System.out.println("No hi ha cap client registrat.");
            } else {
                System.out.println("Aquests són els clients:");
                int contador = 1;
                for (Client client : clients) {
                    System.out.printf("%d. %d - %s\n", contador, client.getDni(), client.getNom());
                    contador++;
                }

                System.out.print("Seleccioni el client per mostrar les seves comandes: ");
                int opcio = Teclado.leerInt(1, clients.size()) - 1;
                int dniClient = clients.get(opcio).getDni();

                query = session.createQuery("FROM Comanda WHERE dni_client = " + dniClient);
                List<Comanda> comandes = query.list();

                if (comandes.isEmpty()) {
                    System.out.println("Aquest client encara no ha fet cap comanda.");
                } else {
                    for (Comanda comanda : comandes)
                        System.out.println(comanda);
                }
            }
        }
    }


    public static void mostrarResum() {
        SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("SELECT C.dni FROM Client C");
            List<Integer> numDNIs = query.list();

            TreeMap<Float, String> llista_preus2 = new TreeMap<>(new Comparator<Float>() {
                @Override
                public int compare(Float o1, Float o2) {
                    return Float.compare(o2, o1);
                }
            });

            Query query2;
            for (Integer numDNI : numDNIs) {
                query = session.createQuery("SELECT C.preu_total FROM Comanda C WHERE dni_client = " + numDNI);
                List<Float> total = query.list();
                query2 = session.createQuery("SELECT C.nom FROM Client C WHERE dni = " + numDNI);
                List<String> nom = query2.list();
                float preu_total = 0;

                for (Float preu : total) {
                    preu_total += preu;
                }
                if (preu_total > 0)
                    llista_preus2.put(preu_total, nom.get(0));
            }

            Iterator<Map.Entry<Float, String>> iterator = llista_preus2.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<Float, String> entry = iterator.next();
                System.out.printf("%s - %.2f\n", entry.getValue(), entry.getKey());
            }
        }
    }
}
